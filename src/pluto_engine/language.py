import time
import json
import threading
import logging
from datetime import datetime

import requests

from .enums import ExecutionState, ConfirmationStatus, ContinuationAction, EventStatus
from .functions import *
from . import LOOP_SLEEP, PLUTO_SERVER_URL


def inform_user(message):
    # to be overwritten by user
    print(message)


def get_event(self, event_name):
    event = self.events.get(event_name)
    if event is not None:
        return event

    # pass request to parent, if element has one
    if self.parent is not None:
        return get_event(self.parent, event_name)

    # otherwise fetch from remote server
    body = {"path": event_name}
    result = requests.post(PLUTO_SERVER_URL + "/event", json=body, timeout=60)
    result = json.loads(result.text)
    # TODO: handle error return
    if result["result"] == "set":
        event = Event(event_name, EventStatus.SET)
    elif result["result"] == "clear":
        event = Event(event_name, EventStatus.CLEAR)
    return event


def get_value(self, path):
    print(f"Fetching reporing data {path} from remote system")

    body = {"path": path}
    result = requests.post(PLUTO_SERVER_URL + "/reporting_data", json=body, timeout=60)
    result = json.loads(result.text)
    reporting_data = ReportingData(path)
    reporting_data.value = result["result"]
    return reporting_data.value


def get_variable(self, variable_name):
    variable = self.variables.get(variable_name)
    if variable is None:
        if self.parent is None:
            raise ValueError(f"Variable {variable_name} not defined")
        return get_variable(self.parent, variable_name)
    return variable


def any_watchdog_active(self):
    for watchdog in self.watchdogs.values():
        if (
            watchdog.execution_state == ExecutionState.EXECUTING
            or watchdog.execution_state == ExecutionState.CONFIRMATION
        ):
            return watchdog
    return False


def execute_activity(path, arguments=None, directives=None):
    body = {"path": path, "arguments": arguments, "directives": directives}
    result = requests.post(PLUTO_SERVER_URL + "/activity", json=body, timeout=60)
    result = json.loads(result.text)
    print(path, arguments, directives)
    # TODO: check return status
    print(f"{path} completed")

    if result["result"] == "confirmed":
        return ConfirmationStatus.CONFIRMED
    elif result["result"] == "not confirmed":
        return ConfirmationStatus.NOT_CONFIRMED
    elif result["result"] == "aborted":
        return ConfirmationStatus.ABORTED
    else:
        raise ValueError


#################################


class ActivityCall:

    def __init__(self, activity, arguments=None, directives=None):
        self.activity = activity
        self.arguments = {}
        for key, expression in arguments.items():
            self.arguments[key] = (
                expression(self) if callable(expression) else expression
            )
        self.directives = {}
        for key, expression in directives.items():
            self.directives[key] = (
                expression(self) if callable(expression) else expression
            )
        self.execution_state = ExecutionState.NOT_INITIATED
        self.confirmation_status = ConfirmationStatus.NOT_AVAILABLE
        self.continuation_action = {
            ConfirmationStatus.CONFIRMED: ContinuationAction.CONTINUE,
            ConfirmationStatus.NOT_CONFIRMED: ContinuationAction.ASK_USER,
            ConfirmationStatus.ABORTED: ContinuationAction.ABORT,
        }

    def run(self):
        self.confirmation_status = ConfirmationStatus.NOT_AVAILABLE
        self.execution_state = ExecutionState.NOT_INITIATED
        self.execution_state = ExecutionState.PRECONDITIONS
        self.execution_state = ExecutionState.ROUTING

        self.execution_state = ExecutionState.EXECUTING

        confirmation_status = execute_activity(
            self.activity, self.arguments, self.directives
        )

        if confirmation_status == ConfirmationStatus.NOT_CONFIRMED:
            self.confirmation_status = ConfirmationStatus.NOT_CONFIRMED
        elif confirmation_status == ConfirmationStatus.ABORTED:
            self.confirmation_status = ConfirmationStatus.ABORTED
        else:
            self.confirmation_status = ConfirmationStatus.CONFIRMED

        self.execution_state = ExecutionState.COMPLETED


class Procedure:

    def __init__(self, parent=None, **kwargs):
        self.parent = parent
        self.kwargs = kwargs

        self.execution_state = ExecutionState.NOT_INITIATED
        self.confirmation_status = ConfirmationStatus.NOT_AVAILABLE
        self.events = {}
        self.variables = {}
        self.watchdogs = {}
        self.refer_by = {}
        self.declaration = []
        self.preconditions = []
        self.main_body = []
        self.watchdog_body = []
        self.confirmation = []
        self.stmt_continuation = None
        self.main_body_finished = False
        self.continuation_action = {
            ConfirmationStatus.CONFIRMED: ContinuationAction.CONTINUE,
            ConfirmationStatus.NOT_CONFIRMED: ContinuationAction.ASK_USER,
            ConfirmationStatus.ABORTED: ContinuationAction.ABORT,
        }

    def __repr__(self):
        return f'Procedure(parent="{self.parent}", kwargs={self.kwargs})'

    def run(self):
        self.confirmation_status = ConfirmationStatus.NOT_AVAILABLE
        self.main_body_finished = False

        self.execution_state = ExecutionState.NOT_INITIATED
        for stmt in self.declaration:
            stmt(self)

        self.execution_state = ExecutionState.PRECONDITIONS
        for stmt in self.preconditions:
            if stmt(self) is False:
                self.execution_state = ExecutionState.COMPLETED
                self.confirmation_status = ConfirmationStatus.ABORTED
                return

        # start watchdog body and main body
        self.execution_state = ExecutionState.EXECUTING
        watchdog_body_thread = threading.Thread(target=self._watchdog_body)
        main_body_thread = threading.Thread(target=self._main_body)

        watchdog_body_thread.start()
        main_body_thread.start()
        main_body_thread.join()
        self.main_body_finished = True

        if self.confirmation_status == ConfirmationStatus.NOT_AVAILABLE:
            self.execution_state = ExecutionState.CONFIRMATION
            for stmt in self.confirmation:
                if stmt(self) is False:
                    self.confirmation_status = ConfirmationStatus.NOT_CONFIRMED
                    break
            else:
                self.confirmation_status = ConfirmationStatus.CONFIRMED

        watchdog_body_thread.join()
        self.execution_state = ExecutionState.COMPLETED

    def _main_body(self):
        for stmt in self.main_body:
            print(stmt)
            self.stmt_continuation = None
            stmt(self)
            # allow time for watchdog to trigger, if any
            time.sleep(LOOP_SLEEP)
            if any_watchdog_active(self):
                self.stmt_continuation = None
                while self.stmt_continuation is None:
                    # wait for any active watchdog to complete
                    time.sleep(LOOP_SLEEP)
            if self.stmt_continuation == ContinuationAction.ABORT:
                self.confirmation_status = ConfirmationStatus.ABORTED
                return
            if self.stmt_continuation == ContinuationAction.TERMINATE:
                return

    def _watchdog_body(self):
        threads = {}
        for stmt in self.watchdog_body:
            thread = threading.Thread(target=stmt, args=[self], daemon=True)
            thread.start()
            threads[stmt] = thread

    def wait_until_absolute_time(self, absolute_time):
        while datetime.utcnow() < absolute_time:
            time.sleep(LOOP_SLEEP)

    def wait_for_relative_time(self, expression):
        time.sleep(expression(self).to("s").magnitude)

    def wait_until_expression(self, expression, timeout=None, raise_event=None):
        if isinstance(expression(self), datetime):
            return self.wait_until_absolute_time(expression(self))

        if timeout is None:
            while self.expression(expression) is False:
                time.sleep(LOOP_SLEEP)
        else:
            end_time = datetime.utcnow() + timeout(self)
            while self.expression(expression) is False:
                # check if timeout
                if datetime.utcnow() > end_time:
                    if raise_event:
                        print(f"!Raising local event {raise_event}!")
                        get_event(self, raise_event).value = EventStatus.SET
                    return False
                time.sleep(LOOP_SLEEP)

    def wait_for_event(self, event, timeout=None, raise_event=None):
        print(get_event(self, event).value, EventStatus.SET)
        if timeout is None:
            while not get_event(self, event).value == EventStatus.SET:
                print(get_event(self, event).value)
                if self.main_body_finished:
                    return False
                time.sleep(LOOP_SLEEP)
        else:
            end_time = datetime.utcnow() + timeout(self)
            while not get_event(self, event).value == EventStatus.SET:
                if self.main_body_finished:
                    return False
                # check if timeout
                if datetime.utcnow() > end_time:
                    if raise_event:
                        print(f"!Raising local event {raise_event}!")
                        get_event(self, raise_event).value = EventStatus.SET
                    return False
                time.sleep(LOOP_SLEEP)

    def expression(self, expression):
        return expression(self)

    def ask_user(self, question):
        answer = input(question + ": ")
        return answer

    def inform_user(self, *expressions):
        message = []
        for expression in expressions:
            message.append(str(expression(self)))
        inform_user(" ".join(message))

    def log(self, *expressions):
        message = []
        for expression in expressions:
            message.append(str(expression(self)))
        logging.info(" ".join(message))

    def initiate_activity(self, activity_call):
        thread = threading.Thread(target=activity_call.run)
        thread.start()

    def initiate_and_confirm_activity(
        self, activity_call, continuation=None, raise_event=None
    ):
        # thread = threading.Thread(target=activity_call.run)
        # thread.start()
        # thread.join()
        activity_call.run()
        confirmation_status = activity_call.confirmation_status

        # init with default continuation actions
        continuation_action = activity_call.continuation_action.get(confirmation_status)
        # if a continuation dict was supplied, get it from there
        if continuation:
            # if the given confirmation status is not included
            # then fall back to the default one
            continuation_action = continuation.get(
                confirmation_status, continuation_action
            )

        if continuation_action == ContinuationAction.RESUME:
            # only used within watchdog; resumes execution of main body
            self.stmt_continuation = ContinuationAction.RESUME

        elif continuation_action == ContinuationAction.ABORT:
            # aborts the procedure
            self.stmt_continuation = ContinuationAction.ABORT

        elif continuation_action == ContinuationAction.RESTART:
            # only used within main body; restarts the statement
            # TODO: handle timeout and max times
            self.initiate_and_confirm_activity(activity_call, continuation, raise_event)

        elif continuation_action == ContinuationAction.ASK_USER:
            while True:
                # ask the user how to proceed
                answer = self.ask_user(
                    "Last statement status: {}.\n"
                    "(a)bort, (r)estart, raise (e)vent, (c)ontinue, "
                    "(t)erminate?".format(confirmation_status)
                ).lower()
                if answer == "a":
                    self.stmt_continuation = ContinuationAction.ABORT
                    break
                if answer == "r":
                    self.initiate_and_confirm_activity(
                        activity_call, continuation, raise_event
                    )
                    break
                if answer == "e":
                    event_name = self.ask_user("Event name")
                    event = get_event(self, event_name)
                    event.value = EventStatus.SET
                    self.stmt_continuation = ContinuationAction.RAISE_EVENT
                    break
                if answer == "c":
                    self.stmt_continuation = ContinuationAction.CONTINUE
                    break
                if answer == "t":
                    self.stmt_continuation = ContinuationAction.TERMINATE
                    break

        elif continuation_action == ContinuationAction.RAISE_EVENT:
            # raise a local event
            event = get_event(self, raise_event) if raise_event else None
            if event:
                event.value = EventStatus.SET
            self.stmt_continuation = ContinuationAction.RAISE_EVENT

        elif continuation_action == ContinuationAction.CONTINUE:
            # only used within main body; continue execution
            self.stmt_continuation = ContinuationAction.CONTINUE

        elif continuation_action == ContinuationAction.TERMINATE:
            # only used within watchdog; end main body and go to confirmation
            self.stmt_continuation = ContinuationAction.TERMINATE

    def initiate_and_confirm_step(self, step, continuation=None, raise_event=None):
        self.initiate_and_confirm_activity(step, continuation, raise_event)

    def initiate_in_parallel_until_one_completes(self, initiate_and_confirm_stmts):
        threads = []
        for stmt in initiate_and_confirm_stmts:
            thread = threading.Thread(target=stmt, args=[self])
            threads.append(thread)
            thread.start()
        while True:
            for thread in threads:
                if not thread.is_alive():
                    return
            time.sleep(LOOP_SLEEP)

    def initiate_in_parallel_until_all_complete(self, initiate_and_confirm_stmts):
        threads = []
        for stmt in initiate_and_confirm_stmts:
            thread = threading.Thread(target=stmt, args=[self])
            threads.append(thread)
            thread.start()
        while True:
            for thread in threads:
                if thread.is_alive():
                    break
            else:
                return
            time.sleep(LOOP_SLEEP)

    def assignement(self, variable_name, expression):
        variable = get_variable(self, variable_name)
        if variable:
            variable.value = expression(self)

    def variable_declaration(self, variable_name, datatype=None):
        var = Variable(variable_name, datatype)
        self.variables[var.name] = var

    def event_declaration(self, event_name, event_description=None):
        event = Event(event_name, event_description)
        self.events[event.name] = event

    def if_statement(self, expression, if_stmts, else_stmts=None):
        step = Step(self)
        if self.expression(expression):
            for stmt in if_stmts:
                step.main_body.append(stmt)
        else:
            for stmt in else_stmts:
                step.main_body.append(stmt)
        self.initiate_and_confirm_step(step)


class Step(Procedure):
    pass


class Event:

    def __init__(self, name, value="clear", description=None):
        self.name = name
        self.value = value
        self.description = description

    def __repr__(self):
        if self.description:
            return f'Event("{self.name}", "{self.description}")'
        return f'Event("{self.name}")'


class Variable:

    def __init__(self, name, value=None, datatype=None, description=None):
        self.name = name
        self.value = value
        self.datatype = datatype
        self.description = description


class ReportingData(Variable):
    pass
