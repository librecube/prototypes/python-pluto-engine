from datetime import datetime
from pint import _DEFAULT_REGISTRY

ureg = _DEFAULT_REGISTRY


def current_time():
    # TODO: allow to offset by given (global) epoch
    return datetime.utcnow()


def year(value=None):
    if value is None:
        return datetime.utcnow().year
    return value.year


def month(value=None):
    if value is None:
        return datetime.utcnow().month
    return value.month


def day_of_month(value=None):
    if value is None:
        return datetime.utcnow().day
    return value.day


def day_of_week(value=None):
    if value is None:
        return datetime.utcnow().weekday()
    return value.weekday()


def day_of_year(value=None):
    if value is None:
        return datetime.utcnow().timetuple().tm_yday
    return value.timetuple().tm_yday


def hour(value=None):
    if value is None:
        return datetime.utcnow().hour
    return value.hour


def minute(value=None):
    if value is None:
        return datetime.utcnow().minute
    return value.minute


def second(value=None):
    if value is None:
        return datetime.utcnow().second
    return value.second


def days(value):
    return value.m_as('days') * ureg.days


def hours(value):
    return value.m_as('hours') * ureg.hours


def minutes(value):
    return value.m_as('minutes') * ureg.hours


def seconds(value):
    return value.m_as('seconds') * ureg.seconds
