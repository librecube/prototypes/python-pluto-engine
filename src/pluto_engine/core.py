import threading
from collections import OrderedDict

from pluto_parser import pluto_parse

from .enums import *
from .language import *
from .functions import *


class PlutoEngine:

    def __init__(self, pluto_server_url=None):
        self._loaded_procedures = {}

    def load(self, filepath, name):
        with open(filepath, "r") as f:
            pluto_script = f.read()
        python_code = pluto_parse(pluto_script)

        self._loaded_procedures[name] = {
            "pluto_script": pluto_script,
            "python_code": python_code,
            "thread": None,
            "instance": None,
        }

    def unload(self, name):
        if name in self._loaded_procedures:
            del self._loaded_procedures[name]

    def list_loaded_procedures(self):
        return list(self._loaded_procedures.keys())

    def run(self, name, **kwargs):
        if name not in self._loaded_procedures:
            raise ValueError()

        python_code = self._loaded_procedures[name]["python_code"]

        # execute the code, which will create the procedure instance
        exec(python_code, globals())

        # capture the procedure object
        instance = globals()["Procedure_"]()

        # pass arguments to procedure instance as variables
        for key, value in kwargs.items():
            instance.variables[key] = Variable(key, value=value)

        t = threading.Thread(target=instance.run)
        self._loaded_procedures[name]["thread"] = t
        self._loaded_procedures[name]["instance"] = instance
        t.start()

    def suspend(self, name):
        pass

    def resume(self, name):
        pass

    def is_running(self, name):
        t = self._loaded_procedures[name]["thread"]
        return t.is_alive()

    def has_completed(self, name):
        t = self._loaded_procedures[name]["thread"]
        instance = self._loaded_procedures[name]["instance"]
        print(instance.confirmation_status, instance.execution_state)
        return not t.is_alive()
