import os


PLUTO_SERVER_URL = os.getenv("PLUTO_SERVER_URL", "http://localhost:8000")
LOOP_SLEEP = 0.5  # the sleep time for loops


from .core import PlutoEngine
