# Test the following capacilities:
# load, unload, activate, stop, resume, report, abort, abort_all
import time

from pluto_engine import PlutoEngine


app = PlutoEngine()

PROCEDURE = "simple.pluto"

# app.load(PROCEDURE, "MyProcedure")
# print(app.list_loaded_procedures())

# app.unload("MyProcedure")
# print(app.list_loaded_procedures())

app.load(PROCEDURE, "MyProcedure")
app.run("MyProcedure")

print("running")
# time.sleep(5)

# app.suspend("MyProcedure")
# print("suspended")
# time.sleep(2)

# app.resume("MyProcedure")
# print("resumed")

while not app.has_completed("MyProcedure"):
    print(".")
    time.sleep(1)

print("completed")
