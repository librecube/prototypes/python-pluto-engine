# This is auto-generated code from the source Pluto file. Do not modify!


class Procedure_(Procedure):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.declaration.append(stmt_pos_30)
        self.declaration.append(stmt_pos_109)
        self.preconditions.append(stmt_pos_219)
        self.main_body.append(stmt_pos_289)
        self.main_body.append(stmt_pos_324)
        self.main_body.append(stmt_pos_368)
        self.main_body.append(stmt_pos_401)
        self.main_body.append(stmt_pos_1250)
        self.main_body.append(stmt_pos_1290)
        self.confirmation.append(stmt_pos_2516)


def stmt_pos_30(caller):
    caller.event_declaration('command_failed', "A remote command did not expected")


def stmt_pos_109(caller):
    caller.event_declaration('bus_switchover', "SpaceCAN bus has switched over")


def stmt_pos_219(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))


def stmt_pos_289(caller):
    caller.log(lambda x: "Starting procedure")


def stmt_pos_324(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = create_activity_call(caller, 'DO_SOMETHING', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_368(caller):
    caller.log(lambda x: "Running step 01")


def stmt_pos_471(caller):
    caller.variable_declaration('speed', int())


def stmt_pos_528(caller):
    caller.variable_declaration('delay', int())


def stmt_pos_635(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))


def stmt_pos_714(caller):
    caller.assignement('delay', lambda x: ureg('5s'))


def stmt_pos_745(caller):
    caller.log(lambda x: "Driving forward")


def stmt_pos_785(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    arguments['SPEED'] = lambda x: ureg('50')
    activity_call = create_activity_call(caller, 'ROVER/WHEEL_DRIVE/DRIVE_FORWARD', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_950(caller):
    caller.wait_for_relative_time(lambda x: get_variable(caller, 'delay').value)


def stmt_pos_983(caller):
    caller.log(lambda x: "Stopping")


def stmt_pos_1016(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = create_activity_call(caller, 'ROVER/WHEEL_DRIVE/STOP', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_1084(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_1161(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))


def stmt_pos_401(caller):
    step = Step_stmt_pos_401(caller)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_401(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.declaration.append(stmt_pos_471)
        self.declaration.append(stmt_pos_528)
        self.preconditions.append(stmt_pos_635)
        self.main_body.append(stmt_pos_714)
        self.main_body.append(stmt_pos_745)
        self.main_body.append(stmt_pos_785)
        self.main_body.append(stmt_pos_950)
        self.main_body.append(stmt_pos_983)
        self.main_body.append(stmt_pos_1016)
        self.main_body.append(stmt_pos_1084)
        self.confirmation.append(stmt_pos_1161)


def stmt_pos_1250(caller):
    caller.log(lambda x: "Running step 02")


def stmt_pos_1360(caller):
    caller.variable_declaration('speed', int())


def stmt_pos_1417(caller):
    caller.variable_declaration('delay', int())


def stmt_pos_1524(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))


def stmt_pos_1603(caller):
    caller.assignement('delay', lambda x: ureg('5s'))


def stmt_pos_1634(caller):
    caller.log(lambda x: "Driving backward")


def stmt_pos_1675(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    arguments['SPEED'] = lambda x: ureg('50')
    activity_call = create_activity_call(caller, 'ROVER/WHEEL_DRIVE/DRIVE_BACKWARD', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_1841(caller):
    caller.wait_for_relative_time(lambda x: get_variable(caller, 'delay').value)


def stmt_pos_1874(caller):
    caller.log(lambda x: "Stopping")


def stmt_pos_1907(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = create_activity_call(caller, 'ROVER/WHEEL_DRIVE/STOP', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_1975(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_2052(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))


def stmt_pos_1290(caller):
    step = Step_stmt_pos_1290(caller)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_1290(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.declaration.append(stmt_pos_1360)
        self.declaration.append(stmt_pos_1417)
        self.preconditions.append(stmt_pos_1524)
        self.main_body.append(stmt_pos_1603)
        self.main_body.append(stmt_pos_1634)
        self.main_body.append(stmt_pos_1675)
        self.main_body.append(stmt_pos_1841)
        self.main_body.append(stmt_pos_1874)
        self.main_body.append(stmt_pos_1907)
        self.main_body.append(stmt_pos_1975)
        self.confirmation.append(stmt_pos_2052)


def stmt_pos_2516(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))
