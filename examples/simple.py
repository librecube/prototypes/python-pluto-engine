# This is auto-generated code from the source Pluto file. Do not modify!


class Procedure_(Procedure):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.declaration.append(stmt_pos_24)
        self.preconditions.append(stmt_pos_115)
        self.preconditions.append(stmt_pos_158)
        self.preconditions.append(stmt_pos_181)
        self.preconditions.append(lambda x: get_value(self, 'MySystem/MyParameter2') == ureg('1'))
        self.main_body.append(stmt_pos_328)
        self.main_body.append(stmt_pos_357)
        self.main_body.append(stmt_pos_430)
        self.main_body.append(stmt_pos_495)
        self.main_body.append(stmt_pos_539)
        self.main_body.append(stmt_pos_622)


def stmt_pos_24(self):
    self.event_declaration('WaitTimeout', "A timeout while waiting")


def stmt_pos_115(self):
    return self.wait_until_expression(lambda x: datetime(2024, 6, 30, 13, 2, 30, 0), timeout=None)


def stmt_pos_158(self):
    return self.wait_for_relative_time(lambda x: ureg('0.1s'))


def stmt_pos_181(self):
    return self.wait_until_expression(lambda x: get_value(self, 'MySystem/MyParameter1') == ureg('1'), timeout=lambda x: ureg('1s'), raise_event='WaitTimeout')


def stmt_pos_328(self):
    return self.log(lambda x: "A log text here.")


def stmt_pos_357(self):
    return self.inform_user(lambda x: "An popup info text here. Procedure continues to run.")


def stmt_pos_430(self):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = ActivityCall('MySystem/MyActivity1', arguments, directives)
    self.initiate_activity(activity_call)


def stmt_pos_495(self):
    return self.inform_user(lambda x: "Starting next activity.")


def stmt_pos_539(self):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = ActivityCall('MySystem/MyActivity2', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    self.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_622(self):
    arguments = OrderedDict()
    directives = OrderedDict()
    arguments['MONITORING'] = lambda x: ureg('2')
    arguments['STATE'] = lambda x: "DISABLE"
    activity_call = ActivityCall('MySystem/MyActivity3', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    continuation[ConfirmationStatus.CONFIRMED] = ContinuationAction.CONTINUE
    continuation[ConfirmationStatus.NOT_CONFIRMED] = ContinuationAction.ABORT
    continuation[ConfirmationStatus.ABORTED] = ContinuationAction.ABORT
    self.refer_by['my_activity_ref'] = self.initiate_and_confirm_activity(activity_call, continuation, raise_event)
